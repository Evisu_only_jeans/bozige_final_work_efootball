# Gitlab共享仓库

管理文件用的，带数据集的用Gitlab方便点。

可能不太稳定，换Gitee也行，不过界面太乱了。

首先要有一个Git Bash，本地~/.ssh/下已有id_rsa.pub文件。

## 下载到本地

``` bash
git clone https://gitlab.com/c4Tch3r/bozige_final_work_efootball.git
```

## 更新文件后推送

``` bash
cd bozige_final_work_efootball
git init
git remote rm origin
git remote add origin git@gitlab.com:c4Tch3r/bozige_final_work_efootball.git
git add .
git commit -m "<id>-Commit" // 输入更新者id
git push -u origin main -f  
```

## 错误提交回退版本
```bash
git log // 查找时间最近版本，记录commitId
git reset --hard <commitId>
git push -f
```

## 开源License MIT