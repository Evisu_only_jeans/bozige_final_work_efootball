# !/usr/bin/env/python
# -*- coding: utf-8 -*-

import random
import time

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def MaxMinNormalization(x, Max, Min):
    x = (x - Min) / (Max - Min)
    return x


def Z_ScoreNormalization(x, mu, sigma):
    x = (x - mu) / sigma
    return x


seed = int(time.time_ns())
random.seed(seed)

np.set_printoptions(suppress=True, precision=20, threshold=10, linewidth=40)  # np禁止科学计数法显示
pd.set_option('display.float_format', lambda x: '%.2f' % x)  # pd禁止科学计数法显示

# 读取CSV文件
df1 = pd.read_csv(r'benign.csv')
df2 = pd.read_csv(r'adware.csv')

# 将两个数据框按行堆叠在一起
df_all = pd.concat([df1, df2])

df_all.columns = df_all.columns.str.replace(' ', '')

df_all = df_all.drop(columns=['FlowID', 'SourceIP', 'SourcePort', 'DestinationIP'])

df_all['Timestamp'] = [int(time.mktime(time.strptime(i, "%d/%m/%Y %H:%M:%S"))) for i in df_all['Timestamp']]

# 根据条件修改 "Label" 列的值
df_all.loc[df_all['Label'] == 'BENIGN', 'Label'] = 0
df_all.loc[df_all['Label'] != 0, 'Label'] = 1

# print(df_all['Label'])
# df_all.drop_duplicates(inplace=True) # 使用drop_duplicates去重，inplace=True对原数据集进行替换
# df_all.reset_index(drop=True, inplace=True) # 删除数据后，恢复索引

df_all_labels = df_all["Label"].copy()  # 复制标签
df_all.drop(["Label"], axis=1, inplace=True)  # 删除逾期标签

# 30%数据做测试集
Xtrain, Xtest, Ytrain, Ytest = train_test_split(df_all, df_all_labels, test_size=0.3, random_state=1)

import torch.nn.functional as F
from torch import nn
import torch

Xtrain_tensor = torch.tensor(torch.from_numpy(Xtrain.values), dtype=torch.float32)
Ytrain_tensor = torch.tensor(Ytrain.values.astype(int), dtype=torch.float32)
Xtest_tensor = torch.tensor(torch.from_numpy(Xtest.values), dtype=torch.float32)
Ytest_tensor = torch.tensor(Ytest.values.astype(int), dtype=torch.float32)


class Model(nn.Module):
    def __init__(self):
        super().__init__()
        self.linear_1 = nn.Linear(80, 256)
        self.linear_2 = nn.Linear(256, 256)
        self.linear_3 = nn.Linear(256, 256)
        self.linear_4 = nn.Linear(256, 1)

    def forward(self, params):
        x = F.relu(self.linear_1(params))
        x = F.relu(self.linear_2(x))
        x = F.relu(self.linear_3(x))
        x = F.sigmoid(self.linear_4(x))
        return x


lr = 0.0001


def get_model():
    model = Model()
    opt = torch.optim.Adam(model.parameters(), lr=lr)
    return model, opt


model, optim = get_model()

loss_function = nn.BCELoss()
batch = 256
no_of_batch = len(df_all) // batch
epoch = 15
for i in range(epoch):
    for j in range(no_of_batch):
        start = i * batch
        end = start + batch
        x = Xtrain_tensor[start:end]
        y = Ytrain_tensor[start:end]
        x = x.numpy()
        for k in range(len(x)):
            x[k] = [MaxMinNormalization(_, x[k].max(), x[k].min()) for _ in x[k]]
        x = torch.tensor(torch.from_numpy(x), dtype=torch.float32)
        yPred = model(x)
        loss = loss_function(yPred, y.reshape(-1, 1))
        optim.zero_grad()
        # loss.backward()
        optim.step()
    with torch.no_grad():
        print('epoch : ', i, ' loss: ', loss_function(model(Xtrain_tensor), Ytrain_tensor.reshape(-1, 1)).data.item())


def getThreshold(tes):
    """
    threshold = floor(tes[0][0] * 100) / 100
    for i in range(len(tes)-1):
        if floor(tes[i][0] * 100) != floor(tes[i+1][0] * 100):
            threshold = max(floor(tes[i][0] * 100), floor(tes[i+1][0] * 100)) / 100
            return threshold
        else:
            continue
    return threshold
    """
    count = 0
    for i in range(len(tes)):
        count += tes[i][0]
    return count / len(tes)


def test(model, Xtest_tensor, Ytest_tensor):
    Xtest_tensor = Xtest_tensor.numpy()
    for k in range(len(Xtest_tensor)):
        Xtest_tensor[k] = [MaxMinNormalization(_, Xtest_tensor[k].max(), Xtest_tensor[k].min()) for _ in
                           Xtest_tensor[k]]
    Xtest_tensor = torch.tensor(torch.from_numpy(Xtest_tensor), dtype=torch.float32)
    Ypred = model(Xtest_tensor)
    threshold = getThreshold(Ypred.detach().numpy())
    Ypred_binary = torch.where(Ypred > threshold, torch.tensor([1.0]), torch.tensor([0.0]))
    print(list(Ypred_binary.detach().numpy()))
    correct = (Ypred_binary == Ytest_tensor.reshape(-1, 1)).sum()
    total = len(Ytest_tensor)
    accuracy = correct.double().item() / total
    return accuracy


accuracy = test(model, Xtest_tensor, Ytest_tensor)
print('Accuracy: {:.2f}%'.format(accuracy * 100))
