import os
import pandas as pd
import numpy as np
import time
from sklearn.model_selection import train_test_split
import random

np.set_printoptions(suppress=True, precision=20, threshold=10, linewidth=40)  # np禁止科学计数法显示
pd.set_option('display.float_format', lambda x: '%.2f' % x)  # pd禁止科学计数法显示

# 定义要读取的文件夹路径
folder_path = r'D:\csv_30000_10'

# 遍历文件夹下所有文件名，筛选出所有 CSV 文件
csv_files = [f for f in os.listdir(folder_path) if f.endswith('.csv')]

# 读取所有 CSV 文件并存储到一个列表中
dfs = []
for csv_file in csv_files:
    csv_path = os.path.join(folder_path, csv_file)
    df = pd.read_csv(csv_path)
    dfs.append(df)

# 对所有 CSV文件进行拼接
df_all = pd.concat(dfs)

df_all.columns = df_all.columns.str.replace(' ', '')

df_all = df_all.drop(columns=['FlowID', 'SourceIP', 'SourcePort', 'DestinationIP'])

# # 对Timestamp的所有值做前后去空格的处理
# df_all['Timestamp'] = df_all['Timestamp'].str.strip()

# 使用正则表达式检查 Timestamp列中的数据是否符合要求
mask = df_all['Timestamp'].str.match(r'\d{2}/\d{2}/\d{4} \d{2}:\d{2}:\d{2}')

# 删除不符合要求的行
df_all = df_all[mask]

# 将Timestamp转成整数
df_all['Timestamp'] = [int(time.mktime(time.strptime(i, "%d/%m/%Y %H:%M:%S"))) for i in df_all['Timestamp']]

# 根据条件修改 "Label" 列的值
df_all.loc[df_all['Label'] == 'BENIGN', 'Label'] = 0
df_all.loc[df_all['Label'] != 0, 'Label'] = 1

df_all.drop_duplicates(inplace=True)  # 使用drop_duplicates去重，inplace=True对原数据集进行替换
df_all.reset_index(drop=True, inplace=True)  # 删除数据后，恢复索引

df_all.to_csv('csv_30000_10.csv', index=False)